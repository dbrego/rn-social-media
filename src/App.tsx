/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState, useEffect, type PropsWithChildren} from 'react';
import {
	Button,
	Linking,
	Platform,
	SafeAreaView,
	ScrollView,
	StatusBar,
	StyleSheet,
	Text,
	useColorScheme,
	View
} from 'react-native';

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';
import {Settings, LoginButton, AccessToken, LoginManager, Profile} from 'react-native-fbsdk-next';

// Setting the facebook app id using setAppID
// Remember to set CFBundleURLSchemes in Info.plist on iOS if needed
Settings.setAppID('460015369458272');
Settings.initializeSDK();

const App = () => {
	const [user, setUser] = useState<any>(null);

	const isDarkMode = useColorScheme() === 'dark';

	useEffect(() => {
		currentProfile();
	}, []);

	const backgroundStyle = {
		backgroundColor: isDarkMode ? Colors.darker : Colors.lighter
	};

	const openFacebook = async () => {
		// const profileName = '159616034235'; //Walmart
		const profileName = '10221323012561694';
		const url = (Platform.OS === 'ios' ? 'fb://profile/' : 'fb://page/') + profileName;
		console.log(url);
		const supported = await Linking.canOpenURL(url);
		console.log(supported);
		if (supported) {
			return Linking.openURL(url);
		} else {
			// return Linking.openURL('https://www.facebook.com/n/?' + profileName);
			const new_url = 'fb://facewebmodal/f?href=' + user.linkURL;

			console.log(new_url);
			return Linking.openURL(new_url);
		}
	};

	const openInstagram = async () => {
		Linking.openURL('http://instagram.com/_u/ninja.400');
	};

	const loginToFacebook = async () => {
		try {
			const result = await LoginManager.logInWithPermissions(['public_profile', 'user_link']);
			console.log('result', result);
			if (result.isCancelled) {
				console.log('Login cancelled');
			} else {
				console.log('Login success with permissions: ' + result?.grantedPermissions?.toString());
				await currentProfile();
			}
		} catch (error) {
			console.log('Login fail with error: ' + error);
		}
	};

	const logoutFacebook = () => {
		LoginManager.logOut();
		setUser(null);
	};

	const currentProfile = async () => {
		const dataToken = await AccessToken.getCurrentAccessToken();
		const token = dataToken?.accessToken;
		console.log('AccessToken: ', token);
		if (!token) return;

		setUser({accessToken: token});

		const profile = await Profile.getCurrentProfile();
		console.log('currentProfile: ', JSON.stringify(profile, null, 1));
		if (profile) {
			console.log('The current logged user is: ', profile.name, '. His profile id is: ', profile.userID);
			let profileLink = profile.linkURL;
			if (!profileLink) {
				const url = 'https://graph.facebook.com/v14.0/me?fields=id%2Cname%2Cemail%2Clink&access_token=' + token;
				const request = await fetch(url);
				const data = await request.json();
				console.log('data', data);
				if (data) {
					profileLink = data.link;
				}
			}

			setUser({...user, ...profile, linkURL: profileLink});
		} else await currentProfile();
	};
	return (
		<SafeAreaView style={backgroundStyle}>
			<StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
			<ScrollView contentInsetAdjustmentBehavior="automatic" style={backgroundStyle}>
				<View
					style={{
						backgroundColor: isDarkMode ? Colors.black : Colors.white
					}}>
					{!user && (
						<View style={{margin: 10}}>
							<Button title="Login To Facebook" onPress={loginToFacebook} />
						</View>
					)}
					{user && (
						<View style={{margin: 10}}>
							<Button title="Logout from Facebook" onPress={logoutFacebook} />
						</View>
					)}
					<View style={{margin: 10}}>
						<Button title="Open facebook" onPress={openFacebook} />
					</View>
					<View style={{margin: 10}}>
						<Button title="Open Instagram" onPress={openInstagram} />
					</View>

					<Text>USER DATA:</Text>
					{user && <Text>{JSON.stringify(user, null, 1)}</Text>}

					{/* <View style={{margin: 10}}>
						<LoginButton
							onLoginFinished={(error, result) => {
								if (error) {
									console.log('login has error: ' + result.error);
								} else if (result.isCancelled) {
									console.log('login is cancelled.');
								} else {
									AccessToken.getCurrentAccessToken().then(data => {
										console.log('AccessToken: ', data?.accessToken.toString());
									});
								}
							}}
							onLogoutFinished={() => console.log('logout.')}
						/>
					</View> */}
				</View>
			</ScrollView>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	sectionContainer: {
		marginTop: 32,
		paddingHorizontal: 24
	},
	sectionTitle: {
		fontSize: 24,
		fontWeight: '600'
	},
	sectionDescription: {
		marginTop: 8,
		fontSize: 18,
		fontWeight: '400'
	},
	highlight: {
		fontWeight: '700'
	}
});

export default App;

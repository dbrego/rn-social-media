module.exports = {
	arrowParens: 'avoid',
	bracketSameLine: true,
	bracketSpacing: false,
	singleQuote: true,
	trailingComma: 'none',
	printWidth: 120,
	tabWidth: 4,
	useTabs: true,
	endOfLine: 'auto'
};
